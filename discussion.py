# Python also allows user to input, with this, users can give inputs to the program.

# [Section] input

#username = input("Please enter your name:\n")
#print(f"Hello {username}! Welcome to python short course!")

#num1 = int(input("Enter first number: "))
#num2 = int(input("Enter second number: "))
#print(f"The sum of num1 and num2 is {num1+num2}")

# [Section] with user inputs, users can give inputs for the program to be used to control the application using control structures.
# Control structures can be divided into selection and repitition control structures

# [Section] If-else statements
# if-else statement are used to choose between two or more code blocks depending on the condition


test_num = 75

if test_num >= 60 :
	print("Test passed!")
else:
	print("Test failed!")

# in python, curly braces  ({}) are not needed to distinguish the code blocks inside the if  or else block. hence, indentations are important as python uses indentation to distinguish parts of code as needed.

#[Section] if else chains can also be used to have more than 2 choices for program

# test_num2 = int(input("Please enter the second number \n"))

# if test_num2 > 0 :
# 	print("The number is positive!")
# 	print(f"The number you provided is {test_num2}")
# elif test_num2 == 0 :
# 	print("the number is 0!")
# else:
# 	print("The number is negative!")

# if test_num2%3 == 0 and test_num2%5 == 0:
# 	print("The number is divisible by 3 and 5")
# elif test_num2%3 == 0:
# 	print("The number is divisible by 3")
# elif test_num2%5 == 0:
# 	print("The number is divisible by 5")
# else:
# 	print(f"{test_num2} is not divisible by 3 nor 5")

# [Section] Loops
# Python has loops thst csn repeat blocks of code
# While loop are used to execute a set of statements as long as the condition is true

# i = 0
# while i <= 5 :
# 	i += 1
# 	print(f"Current value of i is {i}")

# n = 0
# while n <= 5 :
# 	print(f"Current value of n is {n}")	
# 	n += 1

# [Section] for loops are used for iteration over a sequence

fruits = ["apple","banana","cherry"] #lists

for indiv_fruit in fruits:
	print(indiv_fruit)

# [Section] range() methods
# to use the fo loop to iterate through values, the range method can be used.

# for x in range(6):
# 	print(f"the current value of x is {x}!")

# for x in range(6, 10):
# 	print(f"the current value of x is {x}!")

for x in range(6, 20, 2):
	print(f"the current value of x is {x}!")

# [Section] Break statement
# the break statement is used to stop the loop

# j = 1

# while j < 6 :
# 	print(j)
# 	if j==3 :
# 		break

# 	j+=1

# [Section] Continue statement
# it returns the control to the beginning of the while lopp and continue to the next iteration

k = 1
while k < 6 :
	k += 1
	if k == 3:
		continue
	
	print (k)