
year = int(input("Please input a year \n"))

if year > 0 :

	if year%4 == 0 :
		print(f"{year} is a leap year")

	else:
		print(f"{year} is not a leap year")
else:
	print(f"{year} is not valid")



rows = int(input("Enter number of rows\n"))
columns = int(input("Enter number of columns\n"))


column_n=""
asterisk="*"

i=1
n=1

while i <= rows:
	while n <= columns:
		column_n += asterisk[0]
		n += 1
	print(f"{column_n}\n")
	i+=1

